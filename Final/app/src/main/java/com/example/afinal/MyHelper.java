package com.example.afinal;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

class MyHelper extends SQLiteOpenHelper
{
    public MyHelper(Context context){
        super(context, "accountBook_db.db", null, 1);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE accountBook(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                " _year INTEGER NOT NULL, " +
                " _month INTEGER NOT NULL, " +
                " _dayOfMonth INTEGER NOT NULL, " +
                " _type VARCHAR(10) NOT NULL, " +
                " _account REAL NOT NULL, " +
                " _remarks VARCHAR(40) NOT NULL)");
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}