package com.example.afinal;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.icu.util.Calendar;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class DetailedInformationActivity extends AppCompatActivity implements View.OnClickListener{
    MyHelper myHelper;
    private int selectedType = 1;
    private int ID;
    
    private Button btn_date;
    private ImageView iv_type_icon;
    private TextView tv_type_name;
    private EditText et_remarks;
    private TextView tv_account_sign;
    private EditText et_account;

    private ImageButton btn_fuzhuang;
    private ImageButton btn_chuxing;
    private ImageButton btn_zhusu;
    private ImageButton btn_yule;
    private ImageButton btn_sancan;
    private ImageButton btn_lingshi;
    private ImageButton btn_yanjiucha;
    private ImageButton btn_yiliao;
    private ImageButton btn_dianqishuma;
    private ImageButton btn_xuexi;
    private ImageButton btn_zhangdan;
    private ImageButton btn_others_red;
    private ImageButton btn_gongzi;
    private ImageButton btn_licai;
    private ImageButton btn_waikuai;
    private ImageButton btn_others_green;

    private Button btn_delete;
    private Button btn_confirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_information);
        myHelper = new MyHelper(this);
        initUI();
        updateUI();
    }

    private void updateUI() {
        tv_type_name.setText(GlobalVariables.typeIDTypeName(selectedType));
        btn_date.setText(GlobalVariables.selectYear+"/"+GlobalVariables.selectMonth+"/"+GlobalVariables.selectDayOfMonth);
        if(selectedType<20)
        {
            et_account.setTextColor(Color.rgb(204, 102, 102));
            tv_account_sign.setTextColor(Color.rgb(204, 102, 102));
            tv_account_sign.setText("-");
            iv_type_icon.setBackgroundResource(GlobalVariables.typeIDTypeIcon(selectedType));
        }
        else
        {
            et_account.setTextColor(Color.rgb(102, 204, 102));
            tv_account_sign.setTextColor(Color.rgb(102, 204, 102));
            tv_account_sign.setText("+");
            iv_type_icon.setBackgroundResource(GlobalVariables.typeIDTypeIcon(selectedType));
        }
    }

    private void initUI(){
        btn_date = findViewById(R.id.btn_date);
        iv_type_icon = findViewById(R.id.iv_type_icon);
        tv_type_name = findViewById(R.id.tv_type_name);
        et_remarks = findViewById(R.id.et_remarks);
        tv_account_sign = findViewById(R.id.tv_account_sign);
        et_account = findViewById(R.id.et_account);

        btn_fuzhuang = findViewById(R.id.btn_fuzhuang);
        btn_chuxing = findViewById(R.id.btn_chuxing);
        btn_zhusu = findViewById(R.id.btn_zhusu);
        btn_yule = findViewById(R.id.btn_yule);
        btn_sancan = findViewById(R.id.btn_sancan);
        btn_lingshi = findViewById(R.id.btn_lingshi);
        btn_yanjiucha = findViewById(R.id.btn_yanjiucha);
        btn_yiliao = findViewById(R.id.btn_yiliao);
        btn_dianqishuma = findViewById(R.id.btn_dianqishuma);
        btn_xuexi = findViewById(R.id.btn_xuexi);
        btn_zhangdan = findViewById(R.id.btn_zhangdan);
        btn_others_red = findViewById(R.id.btn_others_red);
        btn_gongzi = findViewById(R.id.btn_gongzi);
        btn_licai = findViewById(R.id.btn_licai);
        btn_waikuai = findViewById(R.id.btn_waikuai);
        btn_others_green = findViewById(R.id.btn_others_green);
        et_remarks = findViewById(R.id.et_remarks);
        et_account = findViewById(R.id.et_account);

        btn_delete = findViewById(R.id.btn_delete);
        btn_confirm = findViewById(R.id.btn_confirm);

        btn_date.setOnClickListener(this);
        btn_delete.setOnClickListener(this);
        btn_confirm.setOnClickListener(this);

        btn_fuzhuang .setOnClickListener(this);
        btn_chuxing .setOnClickListener(this);
        btn_zhusu .setOnClickListener(this);
        btn_yule .setOnClickListener(this);
        btn_sancan .setOnClickListener(this);
        btn_lingshi .setOnClickListener(this);
        btn_yanjiucha .setOnClickListener(this);
        btn_yiliao .setOnClickListener(this);
        btn_dianqishuma .setOnClickListener(this);
        btn_xuexi .setOnClickListener(this);
        btn_zhangdan .setOnClickListener(this);
        btn_others_red .setOnClickListener(this);
        btn_gongzi .setOnClickListener(this);
        btn_licai .setOnClickListener(this);
        btn_waikuai .setOnClickListener(this);
        btn_others_green .setOnClickListener(this);

        myHelper = new MyHelper(this);
        SQLiteDatabase db;
        db = myHelper.getReadableDatabase();
        Cursor cursor = db.query("accountBook", null, null, null, null, null, null);
        int num = cursor.getCount();
        int currentID = 0;
        if (num != 0) {
            cursor.moveToFirst();
            Log.e("",cursor.getInt(0)+"");
            if (currentID==GlobalVariables.selectID) {
                GlobalVariables.selectYear=cursor.getInt(1);
                GlobalVariables.selectMonth=cursor.getInt(2);
                GlobalVariables.selectDayOfMonth=cursor.getInt(3);
                iv_type_icon.setBackgroundResource(GlobalVariables.typeIDTypeIcon(cursor.getInt(4)));
                selectedType=cursor.getInt(4);
                et_account.setText(cursor.getString(5));
                et_remarks.setText(cursor.getString(6));
                ID=cursor.getInt(0);
            }
            currentID++;
        }
        while (cursor.moveToNext())
        {
            Log.e("",cursor.getInt(0)+"");
            if (currentID==GlobalVariables.selectID) {
                GlobalVariables.selectYear=cursor.getInt(1);
                GlobalVariables.selectMonth=cursor.getInt(2);
                GlobalVariables.selectDayOfMonth=cursor.getInt(3);
                iv_type_icon.setBackgroundResource(GlobalVariables.typeIDTypeIcon(cursor.getInt(4)));
                selectedType=cursor.getInt(4);
                et_account.setText(String.format("%.2f", cursor.getFloat(5)));
                et_remarks.setText(cursor.getString(6));
                ID=cursor.getInt(0);
            }
            currentID++;
        }
        cursor.close();
        db.close();
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btn_delete:
                deleteData();
                break;
            case R.id.btn_confirm:
                updateData();
                break;
            case R.id.btn_fuzhuang:
                selectedType = 1;
                break;
            case R.id.btn_chuxing:
                selectedType = 2;
                break;
            case R.id.btn_zhusu:
                selectedType = 3;
                break;
            case R.id.btn_yule:
                selectedType = 4;
                break;
            case R.id.btn_sancan:
                selectedType = 5;
                break;
            case R.id.btn_lingshi:
                selectedType = 6;
                break;
            case R.id.btn_yanjiucha:
                selectedType = 7;
                break;
            case R.id.btn_yiliao:
                selectedType = 8;
                break;
            case R.id.btn_dianqishuma:
                selectedType = 9;
                break;
            case R.id.btn_xuexi:
                selectedType = 10;
                break;
            case R.id.btn_zhangdan:
                selectedType = 11;
                break;
            case R.id.btn_others_red:
                selectedType = 12;
                break;
            case R.id.btn_gongzi:
                selectedType = 21;
                break;
            case R.id.btn_licai:
                selectedType = 22;
                break;
            case R.id.btn_waikuai:
                selectedType = 23;
                break;
            case R.id.btn_others_green:
                selectedType = 24;
                break;
            case R.id.btn_date:
                chooseDateActivity(v);
                break;
        }
        updateUI();
    }

    private void updateData() {
        SQLiteDatabase db;
        ContentValues values;
        db = myHelper.getWritableDatabase();
        values = new ContentValues();   //要修改的数据

        values.put("_year", GlobalVariables.selectYear);   //将数据添加到ContentValues对象
        values.put("_month", GlobalVariables.selectMonth);
        values.put("_dayOfMonth", GlobalVariables.selectDayOfMonth);
        values.put("_type", selectedType);
        values.put("_account", Float.parseFloat( et_account.getText().toString()));
        values.put("_remarks", et_remarks.getText().toString());

        db.update("accountBook", values, "_id=?", new String[]{ID+""}); // 更新并得到行数
        Toast.makeText(this, "信息已修改", Toast.LENGTH_SHORT).show();
        db.close();
    }

    public void deleteData(){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("警告");
        builder.setMessage("删除这条数据?");
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.out.println("点了确定");
                delete();
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        //一样要show
        builder.show();
    }
    private void delete(){
        SQLiteDatabase db;
        db = myHelper.getWritableDatabase();
        db.delete("accountBook","_id=?", new String[]{ID+""}); // 更新并得到行数
        Toast.makeText(this, "信息已删除", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(this,MainActivity.class));
        db.close();
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void chooseDateActivity(View v){
        Calendar calendar=Calendar.getInstance();
        new DatePickerDialog( this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                GlobalVariables.selectYear = year;
                GlobalVariables.selectMonth = month+1;
                GlobalVariables.selectDayOfMonth = dayOfMonth;
                updateUI();
            }
        }
                ,calendar.get(Calendar.YEAR)
                ,calendar.get(Calendar.MONTH)
                ,calendar.get(Calendar.DAY_OF_MONTH)).show();
    }
}
