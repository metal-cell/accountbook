package com.example.afinal;

import android.app.DatePickerDialog;
import android.content.Context;
import android.icu.util.Calendar;
import android.os.Build;
import android.view.View;
import android.widget.DatePicker;

import androidx.annotation.RequiresApi;

import java.util.Date;

public abstract class GlobalVariables extends Context {
    public static int selectYear = 2022;  // 选择的年份
    public static int selectMonth = 4; // 选择的月份
    public static int selectDayOfMonth = 4; // 选择的日期
    public static int selectID = 1; // 选择的ID
    public static int typeIDTypeIcon(int i)
    {
        if(i == 1)
            return R.drawable.fuzhuang;
        if(i == 2)
            return R.drawable.chuxing;
        if(i == 3)
            return R.drawable.zhusu;
        if(i == 4)
            return R.drawable.yule;
        if(i == 5)
            return R.drawable.sancan;
        if(i == 6)
            return R.drawable.lingshi;
        if(i == 7)
            return R.drawable.yanjiucha;
        if(i == 8)
            return R.drawable.yiliao;
        if(i == 9)
            return R.drawable.dianqishuma;
        if(i == 10)
            return R.drawable.xuexi;
        if(i == 11)
            return R.drawable.zhangdan;
        if(i == 12)
            return R.drawable.others_red;
        if(i == 21)
            return R.drawable.gongzi;
        if(i == 22)
            return R.drawable.licai;
        if(i == 23)
            return R.drawable.waikuai;
        if(i == 24)
            return R.drawable.others_green;
        return 0;
    }
    public static String typeIDTypeName(int i)
    {
        if(i == 1)
            return "服装";
        if(i == 2)
            return "出行";
        if(i == 3)
            return "住宿";
        if(i == 4)
            return "娱乐";
        if(i == 5)
            return "三餐";
        if(i == 6)
            return "零食";
        if(i == 7)
            return "烟酒茶";
        if(i == 8)
            return "医疗";
        if(i == 9)
            return "电器数码";
        if(i == 10)
            return "学习";
        if(i == 11)
            return "账单";
        if(i == 12)
            return "其它";
        if(i == 21)
            return "工资";
        if(i == 22)
            return "理财";
        if(i == 23)
            return "外快";
        if(i == 24)
            return "其它";
        return null;
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    public static void getCurrentDateActivity(){
        Date currentTime = Calendar.getInstance().getTime();
        GlobalVariables.selectYear = currentTime.getYear() + 1900;
        GlobalVariables.selectMonth = currentTime.getMonth() + 1;
        GlobalVariables.selectDayOfMonth = currentTime.getDate();
    }

}
