package com.example.afinal;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.icu.util.Calendar;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class BookKeepingActivity extends AppCompatActivity implements View.OnClickListener{
    MyHelper myHelper;

    private Button btn_confirm;
    private ImageButton btn_fuzhuang;
    private ImageButton btn_chuxing;
    private ImageButton btn_zhusu;
    private ImageButton btn_yule;
    private ImageButton btn_sancan;
    private ImageButton btn_lingshi;
    private ImageButton btn_yanjiucha;
    private ImageButton btn_yiliao;
    private ImageButton btn_dianqishuma;
    private ImageButton btn_xuexi;
    private ImageButton btn_zhangdan;
    private ImageButton btn_others_red;
    private ImageButton btn_gongzi;
    private ImageButton btn_licai;
    private ImageButton btn_waikuai;
    private ImageButton btn_others_green;
    private EditText et_remarks;
    private Button btn_type;
    private Button btn_select_date;
    private EditText et_account;

    private int selectedType = 1;
    @Override
    @RequiresApi(api = Build.VERSION_CODES.N)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_keeping);
        GlobalVariables.getCurrentDateActivity();
        myHelper = new MyHelper(this);
        initUI();
        updateUI();
    }
    public void initUI(){
        btn_confirm = findViewById(R.id.btn_confirm);
        btn_fuzhuang = findViewById(R.id.btn_fuzhuang);
        btn_chuxing = findViewById(R.id.btn_chuxing);
        btn_zhusu = findViewById(R.id.btn_zhusu);
        btn_yule = findViewById(R.id.btn_yule);
        btn_sancan = findViewById(R.id.btn_sancan);
        btn_lingshi = findViewById(R.id.btn_lingshi);
        btn_yanjiucha = findViewById(R.id.btn_yanjiucha);
        btn_yiliao = findViewById(R.id.btn_yiliao);
        btn_dianqishuma = findViewById(R.id.btn_dianqishuma);
        btn_xuexi = findViewById(R.id.btn_xuexi);
        btn_zhangdan = findViewById(R.id.btn_zhangdan);
        btn_others_red = findViewById(R.id.btn_others_red);
        btn_gongzi = findViewById(R.id.btn_gongzi);
        btn_licai = findViewById(R.id.btn_licai);
        btn_waikuai = findViewById(R.id.btn_waikuai);
        btn_others_green = findViewById(R.id.btn_others_green);
        et_remarks = findViewById(R.id.et_remarks);
        btn_type = findViewById(R.id.btn_type);
        btn_select_date = findViewById(R.id.btn_select_date);
        et_account = findViewById(R.id.et_account);

        btn_confirm .setOnClickListener(this);
        btn_fuzhuang .setOnClickListener(this);
        btn_chuxing .setOnClickListener(this);
        btn_zhusu .setOnClickListener(this);
        btn_yule .setOnClickListener(this);
        btn_sancan .setOnClickListener(this);
        btn_lingshi .setOnClickListener(this);
        btn_yanjiucha .setOnClickListener(this);
        btn_yiliao .setOnClickListener(this);
        btn_dianqishuma .setOnClickListener(this);
        btn_xuexi .setOnClickListener(this);
        btn_zhangdan .setOnClickListener(this);
        btn_others_red .setOnClickListener(this);
        btn_gongzi .setOnClickListener(this);
        btn_licai .setOnClickListener(this);
        btn_waikuai .setOnClickListener(this);
        btn_others_green .setOnClickListener(this);
        btn_type .setOnClickListener(this);
        btn_select_date .setOnClickListener(this);
    }
    public void updateUI(){
        btn_type.setText(GlobalVariables.typeIDTypeName(selectedType));
        btn_select_date.setText(GlobalVariables.selectYear+"/"+GlobalVariables.selectMonth+"/"+GlobalVariables.selectDayOfMonth);
        if(selectedType<20)
            et_account.setTextColor(Color.rgb(204, 102, 102));
        else
            et_account.setTextColor(Color.rgb(102, 204, 102));
    }
    public void DataBase(){
        if(Float.parseFloat( et_account.getText().toString())==0){
            Toast.makeText(this, "金额不能为0", Toast.LENGTH_SHORT).show();
            return;
        }
        SQLiteDatabase db;
        ContentValues values;
        ////////////////////////////////////////////////////
        db = myHelper.getWritableDatabase();    //获取可读写SQLiteDatabase对象
        values = new ContentValues();   //创建ContentValues对象
        ////////////////////////////////////////////////////
        Cursor cursor = db.query("accountBook", null, null, null, null, null, null);
        int num = cursor.getCount();
        if (num != 0) {
            cursor.moveToFirst();
            if(cursor.getInt(1) == GlobalVariables.selectYear && cursor.getInt(2)==GlobalVariables.selectMonth
            && cursor.getInt(3) == GlobalVariables.selectDayOfMonth && cursor.getInt(4)==selectedType
            && cursor.getFloat(5) == Float.parseFloat( et_account.getText().toString())
            && cursor.getString(6).equals(et_remarks.getText().toString()))
            {
                Toast.makeText(this, "信息已存在", Toast.LENGTH_SHORT).show();
                db.close();
                return;
            }
            Log.e("",cursor.getString(6)+"/"+et_remarks.getText().toString());
        }
        while (cursor.moveToNext()) {
            if(cursor.getInt(1)==GlobalVariables.selectYear && cursor.getInt(2) == GlobalVariables.selectMonth
                    && cursor.getInt(3) == GlobalVariables.selectDayOfMonth && cursor.getInt(4)==selectedType
                    && cursor.getFloat(5) == Float.parseFloat( et_account.getText().toString())
                    && cursor.getString(6).equals(et_remarks.getText().toString()))
            {
                Toast.makeText(this, "信息已存在", Toast.LENGTH_SHORT).show();
                db.close();
                return;
            }
            Log.e("",cursor.getString(6)+"/"+et_remarks.getText().toString());
        }
        ////////////////////////////////////////////////////
        values.put("_year", GlobalVariables.selectYear);   //将数据添加到ContentValues对象
        values.put("_month", GlobalVariables.selectMonth);
        values.put("_dayOfMonth", GlobalVariables.selectDayOfMonth);
        values.put("_type", selectedType);
        values.put("_account", Float.parseFloat( et_account.getText().toString()));
        values.put("_remarks", et_remarks.getText().toString());
        ////////////////////////////////////////////////////
        db.insert("accountBook", null, values);
        Toast.makeText(this, "信息已添加", Toast.LENGTH_SHORT).show();
        db.close();
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btn_confirm:
                DataBase();
                break;
            case R.id.btn_fuzhuang:
                selectedType = 1;
                break;
            case R.id.btn_chuxing:
                selectedType = 2;
                break;
            case R.id.btn_zhusu:
                selectedType = 3;
                break;
            case R.id.btn_yule:
                selectedType = 4;
                break;
            case R.id.btn_sancan:
                selectedType = 5;
                break;
            case R.id.btn_lingshi:
                selectedType = 6;
                break;
            case R.id.btn_yanjiucha:
                selectedType = 7;
                break;
            case R.id.btn_yiliao:
                selectedType = 8;
                break;
            case R.id.btn_dianqishuma:
                selectedType = 9;
                break;
            case R.id.btn_xuexi:
                selectedType = 10;
                break;
            case R.id.btn_zhangdan:
                selectedType = 11;
                break;
            case R.id.btn_others_red:
                selectedType = 12;
                break;
            case R.id.btn_gongzi:
                selectedType = 21;
                break;
            case R.id.btn_licai:
                selectedType = 22;
                break;
            case R.id.btn_waikuai:
                selectedType = 23;
                break;
            case R.id.btn_others_green:
                selectedType = 24;
                break;
            case R.id.btn_select_date:
                chooseDateActivity(v);
                break;
        }
        updateUI();
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void chooseDateActivity(View v){
        Calendar calendar=Calendar.getInstance();
        new DatePickerDialog( this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                GlobalVariables.selectYear = year;
                GlobalVariables.selectMonth = month+1;
                GlobalVariables.selectDayOfMonth = dayOfMonth;
                updateUI();
            }
        }
                ,calendar.get(Calendar.YEAR)
                ,calendar.get(Calendar.MONTH)
                ,calendar.get(Calendar.DAY_OF_MONTH)).show();
    }
}
